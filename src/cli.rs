use std::error::Error;

pub enum Action {
	Keep,
	Skip,
}
impl TryFrom<&str> for Action {
	type Error = String;
	fn try_from(s: &str) -> Result<Self, Self::Error> {
		match s {
			"keep" | "take" => Ok(Self::Keep),
			"skip" => Ok(Self::Skip),
			_ => Err(format!("invalid ACTION: {s}")),
		}
	}
}
impl TryFrom<char> for Action {
	type Error = String;
	fn try_from(ch: char) -> Result<Self, Self::Error> {
		match ch {
			'k' | 't' => Ok(Self::Keep),
			's' => Ok(Self::Skip),
			_ => Err(format!("invalid ACTION char: {ch}")),
		}
	}
}

pub enum Portion {
	Every,
	First,
	Last,
}
impl TryFrom<&str> for Portion {
	type Error = String;
	fn try_from(s: &str) -> Result<Self, Self::Error> {
		match s {
			"every" => Ok(Self::Every),
			"first" => Ok(Self::First),
			"last" => Ok(Self::Last),
			_ => Err(format!("invalid PORTION: {s}")),
		}
	}
}
impl TryFrom<char> for Portion {
	type Error = String;
	fn try_from(ch: char) -> Result<Self, Self::Error> {
		match ch {
			'e' => Ok(Self::Every),
			'f' => Ok(Self::First),
			'l' => Ok(Self::Last),
			_ => Err(format!("invalid PORTION char: {ch}")),
		}
	}
}

pub struct Cmd {
	pub action: Action,
	pub portion: Portion,
	pub number: usize,
}

fn parse_standalone_args(args: &[String]) -> Result<(Action, Portion, usize), Box<dyn Error>> {
	let mut args = args.iter();

	let arg1 = args.next().ok_or("no ACTION specified")?;
	let arg2 = args.next().ok_or("no PORTION specified")?;
	let arg3 = args.next().ok_or("no NUMBER specified")?;

	let number = arg3
		.parse::<usize>()
		.map_err(|_| format!("invalid NUMBER: {arg3}"))?;

	let action = Action::try_from(arg1.as_ref())?;
	let portion: Portion = Portion::try_from(arg2.as_str())?;
	Ok((action, portion, number))
}

fn parse_condensed_arg(arg: &str) -> Result<(Action, Portion, usize), Box<dyn Error>> {
	let mut chars = arg.chars();

	let action: Action = chars
		.next()
		.ok_or("condensed arg is empty")
		.map(TryInto::try_into)??;

	let portion = chars.next().unwrap().try_into()?;

	let rest: String = chars.collect();
	let number: usize = rest
		.parse()
		.map_err(|_| format!("invalid integer: {rest}"))?;

	Ok((action, portion, number))
}

pub fn parse_args() -> Result<Cmd, Box<dyn Error>> {
	let args: Vec<String> = std::env::args().skip(1).collect();

	let (action, portion, number) = match args.len() {
		1 => parse_condensed_arg(&args[0])?,
		3 => parse_standalone_args(&args)?,
		_ =>
			return Err(format!(
				"invalid number of arguments: {} (must be 1 or 3)",
				args.len()
			)
			.into()),
	};

	Ok(Cmd {
		action,
		portion,
		number,
	})
}
