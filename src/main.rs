use std::{
	cmp::Ordering,
	collections::VecDeque,
	io::{self, BufReader, BufWriter, IsTerminal, LineWriter, Write},
};

mod cli;
use cli::{Action, Portion};
mod io_ext;
use io_ext::BufReadExt;

struct App;
impl App {
	const HELP: &str = "
   Usage (long form):
      LINES | sift ACTION PORTION NUMBER

   Usage (short form):
      LINES | sift APD[D...]
      where:
         A, P - first character of ACTION or PORTION respectively
         D - NUMBER digits

   ACTION:  (keep | take) | skip
   PORTION: first | last | every
   NUMBER:  an integer > 0";
	fn die(self) -> ! {
		std::process::exit(1);
	}
	fn help(self) -> Self {
		eprintln!("{}", Self::HELP);
		self
	}
	fn err(self, msg: impl std::fmt::Display) -> Self {
		eprintln!("[sift] ERROR: {msg}");
		self
	}
}

fn periodic(
	reader: impl Iterator<Item = Vec<u8>>,
	filterfn: &dyn Fn(usize, &[u8]) -> bool,
	mut output: impl FnMut(&[u8]) -> io::Result<()>,
) {
	reader
		.enumerate()
		.filter(|(idx, bline)| filterfn(*idx, bline))
		.for_each(|(_, bline)| output(&bline).unwrap_or_else(|e| App.err(e).die()));
}

fn get_writer() -> Box<dyn Write> {
	if io::stdout().is_terminal() {
		Box::new(LineWriter::new(io::stdout()))
	} else {
		Box::new(BufWriter::new(io::stdout()))
	}
}

fn main() {
	let cli::Cmd {
		action,
		portion,
		number,
	} = match cli::parse_args() {
		Ok(cmd) => cmd,
		Err(e) => App.err(e).help().die(),
	};

	let reader = BufReader::new(io::stdin()).byte_lines().flatten();
	let mut writer = get_writer();

	let mut output = |data: &[u8]| -> io::Result<()> {
		writer.write_all(data)?;
		writer.write_all(b"\n")?;
		Ok(())
	};

	match action {
		Action::Keep => match portion {
			Portion::Every => {
				periodic(reader, &|idx, _| (idx + 1) % number == 0, output);
			}
			Portion::First => {
				reader
					.take(number)
					.for_each(|bline| output(&bline).unwrap_or_else(|e| App.err(e).die()));
			}
			Portion::Last => {
				let mut buf: VecDeque<Vec<u8>> = VecDeque::with_capacity(number);
				for bline in reader {
					match buf.len().cmp(&number) {
						Ordering::Less => buf.push_back(bline),
						Ordering::Equal => {
							let _ = buf.pop_front();
							buf.push_back(bline);
						}
						Ordering::Greater => unreachable!(),
					}
				}
				buf.iter()
					.for_each(|bline| output(bline).unwrap_or_else(|e| App.err(e).die()));
			}
		},
		Action::Skip => match portion {
			Portion::Every => {
				periodic(reader, &|idx, _| (idx + 1) % number != 0, output);
			}
			Portion::First => {
				reader
					.skip(number)
					.for_each(|bline| output(&bline).unwrap_or_else(|e| App.err(e).die()));
			}
			Portion::Last => {
				let mut buf: VecDeque<Vec<u8>> = VecDeque::with_capacity(number);
				for bline in reader {
					match buf.len().cmp(&number) {
						Ordering::Less => buf.push_back(bline),
						Ordering::Equal => {
							if let Some(bline) = buf.pop_front() {
								output(&bline).unwrap_or_else(|e| App.err(e).die());
							}
							buf.push_back(bline);
						}
						Ordering::Greater => unreachable!(),
					}
				}
			}
		},
	}
}
