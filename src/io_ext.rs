use std::io::{self, BufRead};

pub trait BufReadExt: BufRead {
	fn byte_lines(self) -> BLines<Self>
	where Self: Sized {
		BLines {
			buf: Vec::new(),
			reader: self,
		}
	}
}

pub struct BLines<B> {
	buf: Vec<u8>,
	reader: B,
}

impl<B: BufRead> Iterator for BLines<B> {
	type Item = io::Result<Vec<u8>>;

	fn next(&mut self) -> Option<Self::Item> {
		self.buf.clear();
		match self.reader.read_until(b'\n', &mut self.buf) {
			Err(e) => Some(Err(e)),
			Ok(0) => None,
			Ok(n) => {
				if self.buf[n - 1] == b'\n' {
					self.buf.pop().unwrap();
				}
				Some(Ok(self.buf.to_vec()))
			}
		}
	}
}
impl<B: BufRead> BufReadExt for B {}
