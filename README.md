#### Select portion of lines from the standard input

    Usage (long form):
       LINES | sift ACTION PORTION NUMBER

    Usage (short form):
       LINES | sift APD[D...]
       where:
          A, P - first character of ACTION or PORTION respectively
          D - NUMBER digits

    ACTION:  (keep | take) | skip
    PORTION: first | last | every
    NUMBER:  an integer > 0";
